<?php
include('validar_login.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv= "X-UA-Compatible" content = "IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Side Class - seu site de monitoria</title>

        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" href="./node_modules/bootstrap/dist/css/bootstrap.css">
    </head>

    <body>
        <nav class = "navbar navbar-expand-lg navbar-dark bg-dark navbar-largura">

            <div class = "container">
                <!-- botao toggle -->
                <a class = "navbar-brand" href = "index.php">
                    <span class = "logo" style = "margin-bottom: 10px">Monitoria</span>
                </a>
                <button class ="navbar-toggler" type = "button" data-toggle = "collapse" data-target = "#navbarsite">
                    <span class = "navbar-toggler-icon"></span>
                </button>
                <div class = "collapse navbar-collapse navbar-default" id = "navbarsite">
                    <ul class = "navbar-nav ml-auto">
                        <li class = "nav-item">
                            <a href = "painel.php" class = "nav-link active">Seja bem vindo, &nbsp;<?php echo $_SESSION['user']; ?></a>
                        </li>

                        <li class = "nav-item">
                            <a href = "logout.php" class = "nav-link ">
                                Sair
                            </a>
                        </li>
                    </ul>
                </div>            
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                      <div class="col-2" style="background-color: rgb(0, 255, 153); padding: 0px 0px 30%">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                          <a class="nav-link active" id="v-pills-home-tab" href="#" role="tab" aria-controls="v-pills-home" aria-selected="true" style="font-size: 28px">Home</a>
                          <a class="nav-link" id="v-pills-profile-tab"  href="monitoria.php" role="tab" aria-controls="v-pills-profile" aria-selected="false" style="font-size: 28px">Monitoria</a>
                          <a class="nav-link" id="v-pills-messages-tab" href="tarefas.php" role="tab" aria-controls="v-pills-messages" aria-selected="false" style="font-size: 28px">Tarefas</a>
                          <a class="nav-link" id="v-pills-settings-tab" href="historico.php" role="tab" aria-controls="v-pills-settings" aria-selected="false" style="font-size: 28px">Historico</a>
                        </div>
                      </div>

                      <!--      DIV RESPONSÁVEL PELO CONTEÚDO DA PAGINA     -->
                      <div class="col-10">
                        <div class="container-fluid jumbotron" style="background-color: #FFFFF7; padding: 15px 0px 15px; margin: 10px 0px 10px 0px;">
                            <h2 align="center">    <?php echo $_SESSION['user']?>, seu curso é ... </h2>
                            <a href="inscricaomonitoria.php"><p align="center" class="lead">Inscrever-se para monitor!</p></a>
                        </div>
                        <
                      </div>


                    </div> 
                </div>
            </div>    
        </div>

        <script src="./node_modules/jquery/dist/jquery.slim.min.js"></script>
        <script src="./node_modules/popper.js/dist/popper.min.js"></script>
        <script src="./node_modules/bootstrap/dist/js/bootstrap.js"></script>

    </body>
</html>