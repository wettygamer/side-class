<?php
    session_start();
    if ($_SESSION) {
        header('Location: painel.php');
        exit();
    }
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv= "X-UA-Compatible" content = "IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Side Class - seu site de monitoria</title>

        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" href="./node_modules/bootstrap/dist/css/bootstrap.css">
    </head>

    <body>
        <nav class = "navbar navbar-expand-lg navbar-dark bg-dark navbar-largura mb-3">

            <div class = "container">
                <!-- botao toggle -->
                <a class = "navbar-brand" href = "index.php">
                    <span class = "logo" style = "margin-bottom: 10px">Monitoria</span>
                </a>
                <button class ="navbar-toggler" type = "button" data-toggle = "collapse" data-target = "#navbarsite">
                    <span class = "navbar-toggler-icon"></span>
                </button>
                <div class = "collapse navbar-collapse" id = "navbarsite">
                    <ul class = "navbar-nav mr-auto">
                        <li class = "nav-item"><a href = "Login.html" class = "nav-link">Entrar</a></li>
                        <li class = "nav-item"><a href = "cadastro.html" class = "nav-link">Inscrever-se</a></li>
                    </ul>
                    <ul class = "navbar-nav ml-auto">
                        <li class = "nav-item dropdown">
                            <a class = "nav-link dropdown-toggle" href = "#" data-toggle = "dropdown" id = "navDrop" >
                                Social
                            </a>
                            <div class = "dropdown-menu">
                                <a class = "dropdown-item" href = "#">Facebook</a>
                                <a class = "dropdown-item" href = "#">Instagram</a>
                                <a class = "dropdown-item" href = "#">Twitter</a>
                            </div>
                        </li>
                    </ul>
                    <form class = "form-inline">
                        <input class = "form-control ml-4 mr-2" type = "search" placeholder = "Buscar">
                    </form>
                    <button class = "btn btn-info" type = "submit">Ok
                    </button>
                </div>            
            </div>
        </nav>

        <div id = "carouselSite" class = "carousel slide " data-ride = "carousel">
            <ol class = "carousel-indicators">
                <li data-target = "carouselSite" data-slide-to = "0" class = "active"></li>
                <li data-target = "carouselSite" data-slide-to = "1"></li>
                <li data-target = "carouselSite" data-slide-to = "2"></li>
            </ol>

            <div class = "carousel-inner">

                <div class = "carousel-item active">
                    <center><img src = "css/img/capaSideClass.png"	 class = "img-fluid d-block" style = "width:1230px; height:550px;"/></center>
                    <div class = "carousel-caption d-none d-md-block">
                        <h3>Monitoria SC</h3>
                        <p class = "lead">
                            O Ensino superior para ser realizado com sucesso requer bem mais do que aulas. É                                                   necessário a oferta de diferentes contextos de aprendizagens a fim de favorecer ao aluno um leque de                               oportunidades que privilegie a sua inserção no mundo acadêmico como construtor de seu próprio conhecimento,                         como também, como um aprendiz de outras habilidades e competências que inicialmente não fazem parte de sua                         formação.
                        </p>
                    </div>
                </div>

                <div class = "carousel-item">
                    <center><img src = "imagens/copia1.jpg" class = "img-fluid d-block" style = "width:1230px; height: 550px;"/></center>
                    <div class = "carousel-caption d-none d-md-block">
                        <h3>Universo Fatene 2018</h3>
                        <p class = "lead">          
                            O Ensino superior para ser realizado com sucesso requer bem mais do que aulas. É                                                   necessário a oferta de diferentes contextos de aprendizagens a fim de favorecer ao aluno um leque de                               oportunidades que privilegie a sua inserção no mundo acadêmico como construtor de seu próprio conhecimento,                         como também, como um aprendiz de outras habilidades e competências que inicialmente não fazem parte de sua                         formação.
                        </p>
                    </div>
                </div>

                <div class = "carousel-item">
                    <center><img src = "imagens/im1%20(12).jpg" class = "img-fluid d-block" style = "width:1230px; height: 550px;"/></center>

                    <div class = "carousel-caption d-none d-md-block">
                        <h3>Palestra de Games</h3>
                        <p class = "lead">
                            Os nobres amigos acima promoveram uma palestra de altissimo nível no universo fatene de 2018. Não perca mais tempo, se inscreva agora mesmo em nosso site: www.Vamosinventar.com.br<br> 
                            #GamesSanaFest2019
                        </p>
                    </div>
                </div>
				
				<div class = "carousel-item">
                    <center><img src = "imagens/im1%20(8).jpg" class = "img-fluid d-block" style = "width:1230px; height: 550px;"/></center>

                    <div class = "carousel-caption d-none d-md-block">
                        <h3>titulo1</h3>
                        <p class = "lead">
                            Os nobres amigos acima promoveram uma palestra de altissimo nível no universo fatene de 2018. Não perca mais tempo, se inscreva agora mesmo em nosso site: www.Vamosinventar.com.br<br> 
                            #GamesSanaFest2019
                        </p>
                    </div>
                </div>
				
				<div class = "carousel-item">
                    <center><img src = "imagens/im1%20(11).jpg" class = "img-fluid d-block" style = "width:1230px; height: 550px;"/></center>

                    <div class = "carousel-caption d-none d-md-block">
                        <h3>titulo2</h3>
                        <p class = "lead">
                            Os nobres amigos acima promoveram uma palestra de altissimo nível no universo fatene de 2018. Não perca mais tempo, se inscreva agora mesmo em nosso site: www.Vamosinventar.com.br<br> 
                            #GamesSanaFest2019
                        </p>
                    </div>
                </div>
				
				<div class = "carousel-item">
                    <center><img src = "imagens/im1%20(4).jpg" class = "img-fluid d-block" style = "width:1230px; height: 550px;"/></center>

                    <div class = "carousel-caption d-none d-md-block">
                        <h3>titulo3</h3>
                        <p class = "lead">
                            Os nobres amigos acima promoveram uma palestra de altissimo nível no universo fatene de 2018. Não perca mais tempo, se inscreva agora mesmo em nosso site: www.Vamosinventar.com.br<br> 
                            #GamesSanaFest2019
                        </p>
                    </div>
                </div>
				
				<div class = "carousel-item">
                    <center><img src = "imagens/im1%20(7).jpg" class = "img-fluid d-block" style = "width:1230px; height: 550px;"/></center>

                    <div class = "carousel-caption d-none d-md-block">
                        <h3>titulo 4</h3>
                        <p class = "lead">
                            Os nobres amigos acima promoveram uma palestra de altissimo nível no universo fatene de 2018. Não perca mais tempo, se inscreva agora mesmo em nosso site: www.Vamosinventar.com.br<br> 
                            #GamesSanaFest2019
                        </p>
                    </div>
                </div>
				
				<div class = "carousel-item">
                    <center><img src = "imagens/im1%20(9).jpg" class = "img-fluid d-block" style = "width:1230px; height: 550px;"/></center>

                    <div class = "carousel-caption d-none d-md-block">
                        <h3>titulo5</h3>
                        <p class = "lead">
                            Os nobres amigos acima promoveram uma palestra de altissimo nível no universo fatene de 2018. Não perca mais tempo, se inscreva agora mesmo em nosso site: www.Vamosinventar.com.br<br> 
                            #GamesSanaFest2019
                        </p>
                    </div>
                </div>

            </div>
            <a classs = "carousel-control-prev" href = "#carouselSite" role = "button" data-slide = "prev" >
                <span class = "carousel-control-prev-icon" aria-hidden="true"></span>
                <span class = "sr-only">Anterior</span>
            </a>    
            <a class = "carousel-control-next" href = "#carouselSite" role = "button" data-slide = "next">
                <span class = "carousel-control-next-icon" aria-hidden="true"></span>
                <span class = "sr-only">Avançar</span>
            </a>

        </div>

        <div class = "container"> 
            <div class = "row">
                <div class = "col-12 text-center my-5">
                    <h1 class ="display-3">Bem vindo!</h1>
                    <p class = "lead">Venha fazer parte desse projeto</p>
                </div>
            </div>

            <div class = "row mb-5">
                <div class = "col-3">
                    <nav id = "navbarVertical" class ="navbar navbar-light bg-light">
                        <h5>Disciplinas</h5><br>
                        <nav class = "nav nav-pills flex collumn" >

                            <a class = "nav-link" href = "#item1">Lógica de programação</a>

                            <nav class = "nav nav-pills flex-column">
                                <a class = "nav-link ml-3" href = "#item1-1">Algoritmos</a>
                                <a class = "nav-link ml-3" href = "#item1-1">Estrutura de Dados</a>
                            </nav>

                            <a class = "nav-link my-2" href = "#item2">Banco de Dados</a>
                            <nav class = "nav nav-pills flex-column">
                                <a class = "nav-link ml-3" href = "#item2-1">MySQL</a>
                                <a class = "nav-link ml-3" href = "#item2-1">PHP admin</a>
                            </nav>
                        </nav>
                    </nav>    
                </div>

                <div class = "col-9">
                    <div data-spy = "scroll" data-target = "#navbarVertical" data-offset = "0" class = "scrolspySite">
                        <h4 id = "item1">Conceito - o que é ?</h4>
                        <p class = "lead">é o modo como se escreve um programa de computador, um algoritmo. Um algoritmo é uma                              sequência de passos para se executar uma função. Um exemplo de algoritmo, fora da computação, é uma receita de                      bolo. Na receita, devem-se seguir os passos para o bolo ficar pronto e sem nenhum problema.</p>
                        <h5 id = "item1-1">Exemplos</h5>
                        <p>for(i = 0; i <= 4; i++){
                            System.out.println("informe seu nome:");}
                        </p>
                        <h4 id = "item2">Conceito - o que é ?</h4>
                        <p class = "lead">Bancos de dados ou bases de dados são um conjunto de arquivos relacionados entre si com                           registros sobre pessoas, lugares ou coisas. São coleções organizadas de dados que se relacionam de forma a                         criar algum sentido e dar mais eficiência durante uma pesquisa ou estudo.</p>
                    </div>    
                </div>

            </div>

        </div>



        <script src="./node_modules/jquery/dist/jquery.slim.min.js"></script>
        <script src="./node_modules/popper.js/dist/popper.min.js"></script>
        <script src="./node_modules/bootstrap/dist/js/bootstrap.js"></script>

    </body>
</html>